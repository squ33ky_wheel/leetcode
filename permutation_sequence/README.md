#Small Server App to Serve Permutations On Demand
The [PermutationCalculator](permutation_sequence/src/main/java/permutation_sequence/PermutationCalculator.java)
class is based on a leetcode problem (see below for problem description).
Effectively, that class can output a specific permutation of digits from two inputs, number of digits, and the index of the desired permutation assuming they're sorted.

I chose to wrap this small permutation calculator in a server using spring boot for some practice.


## How to Run
Build and run the server with 
```sh
gradle bootRun
```

Then hit the server with a request such as:
```sh
curl "http://localhost:8080/permutation?numberOfDigits=3&desiredPermutationNumber=3"
# the above command outputs: 
213
```
###Timing a server call
We can see the timing of a server call using curl:
```shell script
curl -w "@curl-format.txt" -s "http://localhost:8080/permutation?numberOfDigits=9&desiredPermutationNumber=10000"
# the above command outputs:
12410957683

    time_namelookup:  0.000001s
       time_connect:  0.000001s
    time_appconnect:  0.000000s
   time_pretransfer:  0.015000s
      time_redirect:  0.000000s
 time_starttransfer:  0.015000s
                    ----------
         time_total:  0.015000s
```

## Run tests
```shell script
gradle build test
```

##Permutation Sequence
Link: https://leetcode.com/problems/permutation-sequence/

###Problem Statement:
Hard

The set [1, 2, 3, ..., n] contains a total of n! unique permutations.

By listing and labeling all of the permutations in order, we get the following sequence for n = 3:

    "123"
    "132"
    "213"
    "231"
    "312"
    "321"

Given n and k, return the kth permutation sequence.

 

Example 1:

Input: n = 3, k = 3
Output: "213"

Example 2:

Input: n = 4, k = 9
Output: "2314"

Example 3:

Input: n = 3, k = 1
Output: "123"

 

Constraints:

    1 <= n <= 9
    1 <= k <= n!


