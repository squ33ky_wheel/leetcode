package permutation_sequence;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class PermutationCalculator {

    // This maps number of digits to the number of possible permutations of those digits.
    protected Map<Integer, Long> mapNumbersToNumPermutations = new ConcurrentHashMap<>();
    PermutationCalculator() {
        // Initialize the first few permutation counts
        mapNumbersToNumPermutations.put(1, 1L);
        mapNumbersToNumPermutations.put(2, 2L);
        mapNumbersToNumPermutations.put(3, 6L);
    }


    public String getPermutation(int numberOfDigits, long permutationNumber) {
        if (numberOfDigits < 1 || permutationNumber < 1) {
            throw new IllegalArgumentException("Both numberOfDigits and permutationNumber values must be non-zero positive integers.");
        }

        if (numberOfDigits > 9) {
            throw new IllegalArgumentException("Can't use more than 10 digits (since there aren't any in base 10)");
        }

        ArrayList<Integer> availableNumbers = new ArrayList<>(numberOfDigits);
        for(int i=1; i <= numberOfDigits; i++) {
            availableNumbers.add(i);
        }

        String toReturn = "";
        long targetPermutationNumber = permutationNumber - 1;
        while(availableNumbers.size() > 1) {
            log.debug(String.format("top of loop: targetPerm:%s toReturn:%s", targetPermutationNumber, toReturn));
            long numPermutationsOfOneLess = getNumPermutations(availableNumbers.size() - 1);

            long targetNumberIndex = (targetPermutationNumber / numPermutationsOfOneLess);
            targetPermutationNumber -= targetNumberIndex * numPermutationsOfOneLess;

            log.debug("targetIndex: " + targetNumberIndex);
            toReturn += availableNumbers.get(Math.toIntExact(targetNumberIndex)).toString();
            availableNumbers.remove((int)targetNumberIndex);
        }

        // Should only be one element left!
        if (availableNumbers.size() != 1)
        {
            log.debug("Have a different number of available numbers left: " + availableNumbers.size());
        }
        toReturn += availableNumbers.get(0);

        return toReturn;
    }

    protected long getNumPermutations(int remainingNumbers) {
        if (mapNumbersToNumPermutations.containsKey(remainingNumbers)) {
            return mapNumbersToNumPermutations.get(remainingNumbers);
        }

        long aggregateCountOfPermutations = 1;
        for(long i = 1; i <= remainingNumbers; i++) {
            aggregateCountOfPermutations = Math.multiplyExact(aggregateCountOfPermutations, i);
        }

        // Save this new value to the map
        mapNumbersToNumPermutations.put(remainingNumbers, aggregateCountOfPermutations);
        return aggregateCountOfPermutations;
    }
}