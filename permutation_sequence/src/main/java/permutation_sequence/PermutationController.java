package permutation_sequence;

import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequiredArgsConstructor
public class PermutationController {

    private final PermutationCalculator permutationCalculator;

    @GetMapping("/permutation")
    @Cacheable("permutations")
    public String fetchPermutation(@RequestParam int numberOfDigits, @RequestParam long desiredPermutationNumber) {
        return permutationCalculator.getPermutation(numberOfDigits, desiredPermutationNumber);
    }
}
