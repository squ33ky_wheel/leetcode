package permutation_sequence;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AppTest {

    @Autowired
    private MockMvc mvc;

    @Test
    void contextLoads() {
    }

    @Test
    void testGetMappingForPermutations() throws Exception {
        PermutationCalculator permutationCalculator = new PermutationCalculator();
        String expectedPermutation = permutationCalculator.getPermutation(3, 3);
        mvc.perform(MockMvcRequestBuilders.get("/permutation?numberOfDigits=3&desiredPermutationNumber=3"))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo(expectedPermutation)));
    }

}