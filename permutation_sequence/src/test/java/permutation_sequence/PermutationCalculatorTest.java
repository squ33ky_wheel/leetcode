package permutation_sequence;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.*;

public class PermutationCalculatorTest {
    PermutationCalculator permutationCalculator;

    @BeforeEach
    void setup() {
        permutationCalculator = new PermutationCalculator();
    }

    @Test
    void simpleTest() {
        assertEquals("213", permutationCalculator.getPermutation(3, 3));
    }

    @Test
    void moreComplexTest() {
        assertEquals("2314", permutationCalculator.getPermutation(4, 9));
    }

    @Test
    void example3Test() {
        assertEquals("123", permutationCalculator.getPermutation(3, 1));
    }

    @Test
    void testNewPermutationMethod() {
        int expectedNumberOfValues = 20;
        for (int i = 1; i <= expectedNumberOfValues; i++) {
            assertEquals(permutationCalculator.getNumPermutations(i), permutationCalculator.getNumPermutations(i));
        }
        //Confirm that we add info to the map.
        assertEquals(expectedNumberOfValues, permutationCalculator.mapNumbersToNumPermutations.size());
    }

    @Test
    void testInvalidInputs() {
        assertThrows(IllegalArgumentException.class, () -> {
            permutationCalculator.getPermutation(-1, 3);
        });

        assertThrows(IllegalArgumentException.class, () -> {
            permutationCalculator.getPermutation(0, 0);
        });
    }

    @Test
    void testTooManyDigits() {
        assertThrows(IllegalArgumentException.class, () -> {
            permutationCalculator.getPermutation(100, 20000);
        });
    }


    /**
     * The value of this test is pretty meh, but I wanted to visually confirm performance change.
     * For instance locally I see:
     * " First call took PT0.0318504S. Second took PT0.0030231S, Third took PT0.0033681S "
     * Showing a 10x increase for the second run of this same call, with gains being consistent for more calls.
     */
    @Test
    void testSpeedWithMemoization() {
        long firstTime = System.nanoTime();
        int numberOfDigits = 9;
        int permutationNumber = 100;
        String firstPass = permutationCalculator.getPermutation(numberOfDigits, permutationNumber);
        long secondTime = System.nanoTime();
        String secondPass = permutationCalculator.getPermutation(numberOfDigits, permutationNumber);
        long thirdTime = System.nanoTime();
        String thirdPass = permutationCalculator.getPermutation(numberOfDigits, permutationNumber);
        long fourthTime = System.nanoTime();

        assertEquals(firstPass, secondPass);
        assertEquals(firstPass, thirdPass);

        Duration firstCallDuration = Duration.ofNanos(secondTime - firstTime);
        Duration secondCallDuration = Duration.ofNanos(thirdTime - secondTime);
        Duration thirdCallDuration = Duration.ofNanos(fourthTime - thirdTime);

        System.out.println(String.format("First call took %s. Second took %s, Third took %s", firstCallDuration, secondCallDuration, thirdCallDuration));
        // second call should be smaller (faster) than first
        assertTrue(secondCallDuration.compareTo(firstCallDuration) < 0);
        assertTrue(thirdCallDuration.compareTo(firstCallDuration) < 0);

    }
}
